import { NgModule } from '@angular/core';
import { GroupsComponent } from './groups/groups';
@NgModule({
	declarations: [GroupsComponent],
	imports: [],
	exports: [GroupsComponent]
})
export class ComponentsModule {}
