import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Api } from '../api/api';
import { OnInit } from '@angular/core';

/*
  Generated class for the GroupserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GroupserviceProvider {
  seq: any = {};
  postit: any = {};
  constructor(public api: Api) {

  }

  getFollowingGroups(email) {
    let request = this.api.get('groupsByMember?email=' + email).share();
    return request;
  }

  getAvailableGroups(email) {
    let request = this.api.get('available').share();
    return request;
  }

  createGroup(group_object) {
    let request = this.api.post('group', group_object).share();
    return request;
  }

  deleteGroup(groupId) {
    let req = this.api.delete('group/delete/'+groupId).share();
    return req;
  }

  joinGroup(groupName, user) {
    let data: any = {};
    data.name = groupName;
    data.user = user;
    let request = this.api.post('JoinGroup', data).share();
    return request;
  }

  getPosts(){
    let request = this.api.get('post',[]).share();
    return request;
    
  }

  leaveGroup(groupName, userEmail) {
    let data: any = {};
    data.name = groupName;
    data.email = userEmail;
    let request = this.api.post('leaveGroup', data).share();
    return request;
  }

 

}
