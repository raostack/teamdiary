import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { MyApp } from '../../app/app.component';

/**
 * Generated class for the OfflinePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-offline',
  templateUrl: 'offline.html',
})
export class OfflinePage {

  constructor(private myApp: MyApp,public navCtrl: NavController, public navParams: NavParams,public platform: Platform) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OfflinePage');
  }

  closeApp()
  {
  	this.platform.exitApp();
  }

  retry()
  {
  		this.myApp.checkNetwork();
  }

}
