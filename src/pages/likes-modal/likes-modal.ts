import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the LikesModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-likes-modal',
  templateUrl: 'likes-modal.html',
})
export class LikesModalPage {
  likesData=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LikesModalPage');
    console.log(this.navParams.data)
    this.likesData=this.navParams.data.data;
    console.log(this.likesData)
  }

}
