import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker
 } from '@ionic-native/google-maps';

 declare var google;
@IonicPage()
@Component({
  selector: 'page-content',
  templateUrl: 'content.html'
})
export class ContentPage {
  @ViewChild('map') 
  mapElement: ElementRef;

  map: any;
  constructor(private googleMaps: GoogleMaps) { }

  ionViewDidLoad() {
   this.loadMap();
  }

 loadMap() {

  let latLng = new google.maps.LatLng(-34.9290, 138.6010);
  
     let mapOptions = {
       center: latLng,
       zoom: 15,
       mapTypeId: google.maps.MapTypeId.ROADMAP
     }
  
     this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    
  }

}
