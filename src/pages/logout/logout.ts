import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { User } from '../../providers/user/user'

import { MainPage } from '../pages';

/**
 * Generated class for the LogoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-logout',
  templateUrl: 'logout.html',
})
export class LogoutPage {
  curuser:any={};
  constructor(private user:User,public navCtrl: NavController, public navParams: NavParams,public storage: Storage) {
    
    storage.get('user').then((res)=>{
      user.api.get('user/'+res.email).subscribe((usr)=>{
        this.curuser=usr;
        console.log(usr)
      })
      
      // this.imgurl=res.imageurl;
      
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LogoutPage');
  }

  goToLogin()
  {
  	this.navCtrl.setRoot('LoginPage');
  }

  goToMainPage(){
    this.navCtrl.setRoot(MainPage);
    }

  removeUser()
  {
  	  this.storage.set('token',null);
  	  this.storage.set('user',null);
  	  this.goToLogin();	
  }

  signUp()
  {
  	this.navCtrl.setRoot('SignupPage');
  }

}
