import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';

import { User } from '../../providers/providers';
import { MainPage } from '../pages';
import { Api } from '../../providers/api/api';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})

export class LoginPage {

  isLoggedIn: boolean;
  fbUserData: any;
  errorMessage: any = "";

  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { email: string, password: string } = {
    email: '',
    password: ''
  };
  isDisabled: boolean = true;
  response: any = {};

  // Our translated text strings
  private loginErrorString: string;

  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService,
    public storage: Storage,
    public api: Api,
    private fb: Facebook) {

    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })
  }

  facebookLogin() {
    this.fb.login(['public_profile', 'user_friends', 'email'])
      .then(res => {
        if (res.status === "connected") {
          this.isLoggedIn = true;
          this.getUserDetail(res.authResponse.userID);

          //to go to MainPage
          this.navCtrl.setRoot(MainPage);
        }
        else {
          this.isLoggedIn = false;
        }
      })
      .catch(e => console.log('Error logging into Facebook', e));
  }

  getUserDetail(fbUserData) {
    console.log(this.fbUserData);
  }

  goToForgotPassword() {
    this.navCtrl.push('ForgotPasswordPage', {});
  }

  goToSignupPage() {
    this.navCtrl.push('SignupPage', {});
  }

  enableLogin() {
    this.isDisabled = false;
  }

  // Attempt to login in through our User service
  doLogin() {
    this.user.login(this.account).subscribe((resp) => {
      this.response = resp;
      if(this.response.message == 'Authentication failed. User not found.')
      {
          this.errorMessage = "Invalid Credentials";
      }
      if (this.response.token && this.response.token !== '') {
        // set a key/value
        this.storage.set('token', this.response.token);
        this.storage.set('user', this.response.user);

        this.storage.get('token').then((token) => {
          if (token != null) {
            this.api.setToken(token);
          }
        });

        //to go to MainPage
        this.navCtrl.setRoot(MainPage);
      }
    }, (err) => {
      // Unable to log in
      let toast = this.toastCtrl.create({
        message: this.loginErrorString,
        duration: 5000,
        position: 'top'
      });
      toast.present();
    });
  }
}
