import { Component ,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController,ViewController,PopoverController,Content } from 'ionic-angular';
import { Api } from '../../providers/api/api';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the FeedsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feeds',
  templateUrl: 'feeds.html',
})

export class FeedsPage {
  @ViewChild(Content) content: Content;
  commentData={
    comment:"",
    user:{},
    postid:""
  };
  likeData={
    user:{},
    postid:""
  };
  postsData:any=[];
  isLoggedIn=false;
  hasPostsData=false;
  comment=[];
  like=[];
  commentsData=null;
  likesData=null;
  moreComment=false;
  user;
  hasBeenLiked=[];
  constructor(public storage: Storage, public api : Api,private modalCtrl: ModalController,public navCtrl: NavController, public navParams: NavParams,public popoverCtrl: PopoverController) 
  {

    storage.get('user').then((user) => {
      if(user)
      {
        this.isLoggedIn=true;
        this.user=user;
      }
      console.log("User present in storage",user)
    },error=>console.log(error));

    
  }
  presentPopover(myEvent,index,data) {
    
    let popover = this.popoverCtrl.create('LikepopoverPage',{data:data});
    myEvent.target.getBoundingClientRect = function(){
      return {top: '100'};
    }
    popover.present({
      ev: myEvent
    });
    return true;
  }
  // tempFunctionLikesPopup(){
  //   let popover = this.popoverCtrl.create('LikepopoverPage');
  //   popover.present();
  //   return true;
  // }
  postComment(postid,index){
    // console.log(event.target.querySelector(".txtComment"));
    
    this.commentData.user=this.user;
    this.commentData.postid=postid;
    this.api.post("post/comment",this.commentData,{}).subscribe((data)=>console.log("Success in comment ",data),(error)=>console.log(error));
    this.postsData[index].commentsCount++;
    this.commentData={
      comment:"",
      user:{},
      postid:""
    };
  }
  postLike(postid,index){
    if(this.postsData[index].hasBeenLiked==false)
    {

      this.likeData.user=this.user;
      this.likeData.postid=postid;
      this.api.post("post/like",this.likeData,{}).subscribe((data)=>console.log("Success in likes :",data),(error)=>console.log(error));
      this.postsData[index].likesCount++;
      this.likeData={
        user:{},
        postid:""
      };
      this.postsData[index].hasBeenLiked=true;
    }
    else
    {
      
    }
  }
  showComment(index,postid){
    console.log("showcomment ID : "+postid)
    this.comment[index]=!this.comment[index];
    if(this.comment[index])
    {
      this.api.get("post/comments?postid="+postid).subscribe((data)=>{
        console.log("Comments show: ",data)
        this.commentsData=data;
      },(error)=>{console.log(error)})
    }
    if(this.postsData[index].comments.length>3){
      this.moreComment=true;
    }
    else
      this.moreComment=false;
    console.log("More Comments : ",this.moreComment);
  }
  showLike(event,index,postid){
    console.log("showlikes  ID : "+postid)
   
      this.api.get("post/likes?postid="+postid).subscribe((data)=>{
        console.log("Likes show: ",data)
        this.likesData=data;
        if(this.likesData.length>0)
        {
          this.presentPopover(event,index,this.likesData)
        }
        
      },(error)=>{console.log(error)})
    }
    
  showModal(){
    const profileModal = this.modalCtrl.create('NewpostPage');
    profileModal.present();
    profileModal.onDidDismiss(data => {
      if(data.hasPosted==true)
        this.fetchPosts();
      // data.likesCount=0;
      // data.commentsCount=0;
      // data.likes=[];
      // data.comments=[];
      // data.updated=new Date();
      // console.log(data);
      // this.postsData.unshift(data);

    });
  }
  displayComments(postIndex){
    //console.log(this.postsData[postIndex].comments);
    // this.postsData[postIndex].comments=[{
    //   comment:"Hi there",
    //   user: {
    //     name:"Akshay",
    //     email:"akshay@gmail.com"
    //   },
    //   date:"May 21 2016"

    // }]
    this.navCtrl.push('CommentsPage',{data:this.postsData[postIndex].comments});
    
  }

  fetchPosts()
  {
    console.log("Fetching posts...");
    this.api.get("post").subscribe(data=>{
      this.hasPostsData=true;
      console.log(data);
      // console.log("Point1");
      this.postsData=data;
      for(let i=0;i<this.postsData.length;i++)
      {
        // if(this.postsData[i].likes != null)
        //   this.postsData[i].likesCount=this.postsData[i].likes.length;
        // else
        //   this.postsData[i].likesCount=0;

        // if(this.postsData[i].comments != null)
        //   this.postsData[i].commentsCount=this.postsData[i].comments.length;
        // else
        //   this.postsData[i].commentsCount=0;
        this.like[i]=false;
        this.comment[i]=false;

        this.api.get("post/comments?postid="+this.postsData[i]._id).subscribe((data)=>{
          // console.log("Comments : ",data)
          this.postsData[i].comments=data;
          if(this.postsData[i].comments==null)
            this.postsData[i].commentsCount=0;
          else
          this.postsData[i].commentsCount=this.postsData[i].comments.length;
        },(error)=>{console.log(error)})

        this.api.get("post/likes?postid="+this.postsData[i]._id).subscribe((data)=>{
          // console.log("Likes : ",data)
          this.postsData[i].likes=data;
          this.postsData[i].hasBeenLiked=false;
          for(let j=0;j<this.postsData[i].likes.length;j++)
          {
            if(this.user._id==this.postsData[i].likes[j].user._id)
              this.postsData[i].hasBeenLiked=true;
          }
          
          if(this.postsData[i].likes==null)
            this.postsData[i].likesCount=0;
          else
          this.postsData[i].likesCount=this.postsData[i].likes.length;
        },(error)=>{console.log(error)})
      }
      console.log("Refined data : ",this.postsData)
      
    }
      ,error=>console.log(error));
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad FeedsPage');
    this.fetchPosts();
  }

}

// @Component({
//   template: `
//     <ion-list>
//       <ion-list-header>Ionic</ion-list-header>
//       <button ion-item (click)="close()">Learn Ionic</button>
//       <button ion-item (click)="close()">Documentation</button>
//       <button ion-item (click)="close()">Showcase</button>
//       <button ion-item (click)="close()">GitHub Repo</button>
//     </ion-list>
//   `
// })
// class PopoverPage {
//   constructor(public viewCtrl: ViewController) {}

//   close() {
//     this.viewCtrl.dismiss();
//   }
// }
