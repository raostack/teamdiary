import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { ToastController,ViewController } from 'ionic-angular';


import {Storage} from '@ionic/storage'
import { Settings } from '../../providers/providers';
import { User } from '../../providers/user/user'
import { MainPage } from '../pages';


/**
 * The Settings page is a simple form that syncs with a Settings provider
 * to enable the user to customize settings for the app.
 *
 */
@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  // Our local settings object
  fabicon:string="fa-pencil"
  options: any;
  public page: string ='profile';
  settingsReady = false;
  edit: boolean=false;
  sameUser: boolean=true;
  profile:any={}
  form: FormGroup;

  profileSettings = {
    page: 'profile',
    pageTitleKey: 'SETTINGS_PAGE_PROFILE'
  };

  pageTitleKey: string = 'SETTINGS_TITLE';
  pageTitle: string;

  subSettings: any = SettingsPage;

  constructor(public viewCtrl:ViewController,
    public toast:ToastController,
    public notification:LocalNotifications,
    private camera:Camera,
    public storage:Storage,
    public user:User,
    public navCtrl: NavController,
    public settings: Settings,
    public formBuilder: FormBuilder,
    public navParams: NavParams,
    public translate: TranslateService) {
      // console.log(navCtrl.getPrevious().name)
      
      if(navCtrl.getPrevious())
        if(navCtrl.getPrevious().name=="WelcomePage"){
          this.edit=true;
          // this._buildForm();
        }
      // INIT Profile.get() from Server
      // this._buildForm();
      var dispEmail=navParams.get('email')
      console.log(dispEmail)
      if(!dispEmail)
        storage.get('user').then((val)=>{
          console.log(val);
          user.api.get('user/'+val.email).subscribe((val)=>{
            if(val)
              // console.log(val)
              this.profile=val
              // console.log(this.profile)
              this._buildForm()
            })
        })
        else{
          this.sameUser=false;
          user.api.get('user/'+dispEmail).subscribe((val)=>{
            if(val)
              // console.log(val)
              this.profile=val
              // console.log(this.profile)
              this._buildForm()
            })
          }
      
      // storage.get('users').then(val=>this.profile=val);

  }
  back(){
    if(this.navCtrl.getPrevious())
      this.navCtrl.pop()
    else
      this.navCtrl.setRoot(MainPage)
  }
  toggleEdit(){
    if(this.fabicon=="fa-check")
      {
        this.fabicon="fa-pencil"
        this.saveProfile();
      }
    else
      this.fabicon="fa-check"
    this.edit=!this.edit;
  }
  toggleSameUser(){
    this.sameUser=!this.sameUser;
    console.log(this.sameUser)
  }
  setImageCamera(){
    console.log('in')
    if(this.edit==true){
      const options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        sourceType :this.camera.PictureSourceType.CAMERA,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      }

      this.camera.getPicture(options).then((imageuri)=>{
        
        this.uploadImage(this.profile.imageurl)
      })
    }
  }
  notify(){
    this.notification.schedule({
      id: 1,
      text: 'Single ILocalNotification',
      sound: null
    })
  }
  setImageGallery(){
    console.log('in')
    if(this.edit==true){
      const options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        sourceType :this.camera.PictureSourceType.PHOTOLIBRARY,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      }

      this.camera.getPicture(options).then((imageuri)=>{
        
        this.uploadImage(this.profile.imageurl)
      })
    }
  }
  uploadImage(img){
    this.profile.imageurl=img;
  }
  
  saveProfile(){
    var passwd:string=this.profile.password;
    var x=this;
    var frm=this.form.value;
    var img=this.profile.imageurl;
    // console.log(frm)
    //TODO: Insert Profile Save Code here profile.set(User Data)
    console.log(this.profile.password==frm.password)
    if(this.form.value.newPass!="" && this.form.value.newPass){
      if(frm.newPass==frm.confPass){
        if(frm.password==this.profile.password)
          passwd=frm.newPass;
        else
          alert('Invalid Password');
         
          return;
      }
      else{
        alert("Old Pass must match new pass")
        
        console.log(frm.newPass+" "+frm.confPass);
        
        return;
      }
    }
    
    
    this.profile=frm;
    this.profile.password=passwd
    // this.pr
    // this.storage.set
    // this.profile.name=frm.name;
    // this.profile=frm;
    
    delete this.profile.newPass;
    delete this.profile.confPass;
    // console.log(this.pr);
    console.log(this.profile);
    if(!img)
      this.profile.imageurl="/assets/img/dp.jpg"
    else
      this.profile.imageurl=img;
    this.user.update(this.profile);

    // this.user.update(this.profile);
    
    // this.user.api.post('/data',this.form.value)
    if(x.navCtrl.getPrevious())
      if(x.navCtrl.getPrevious().name=="SignupPage")
        this.navCtrl.push(MainPage).then(function(){
          
      });
      else{
        x.navCtrl.pop();
      }
    else{
      x.navCtrl.setRoot(MainPage);
    }
    
    
  }

  _buildForm() {
    let group: any = {
      option1: [this.options.option1],
      option2: [this.options.option2],
      option3: [this.options.option3],
      
      
    };
    console.log(this.page)
    switch (this.page) {
      case 'main':
        break;
      case 'profile':
        group = {
          name: [this.profile.name],
          gender: [this.profile.gender],
          aboutme: [this.profile.aboutme],
          tel: [this.profile.tel],
          dob: [this.profile.dob],
          city: [this.profile.city],
          password: [''],
          newPass:[''],
          confPass: [''],
          _id:[this.profile._id],
          email:[this.profile.email],
          imageurl:[this.profile.imageurl]
        };
        
        break;
        default: console.log('default'); break;
    }
    this.form = this.formBuilder.group(group);

    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.settings.merge(this.form.value);
    });
  }

  ionViewDidLoad() {
    // Build an empty form for the template to render
    this.form = this.formBuilder.group({});
    // if(!this.viewCtrl.isFirst()){
    //   this.viewCtrl.showBackButton(true);

    // }
    
  }

  ionViewWillEnter() {
    // Build an empty form for the template to render
    this.form = this.formBuilder.group({});

    this.page = this.navParams.get('page') || this.page;
    this.pageTitleKey = this.navParams.get('pageTitleKey') || this.pageTitleKey;

    this.translate.get(this.pageTitleKey).subscribe((res) => {
      this.pageTitle = res;
    })

    this.settings.load().then(() => {
      this.settingsReady = true;
      this.options = this.settings.allSettings;

      this._buildForm();
    });
  }

  ngOnChanges() {
    console.log('Ng All Changes');
  }
}
