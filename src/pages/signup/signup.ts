import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

import { User } from '../../providers/providers';
import { MainPage } from '../pages';
import { Storage } from '@ionic/storage';
import { SettingsPage } from '../settings/settings';


@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})

export class SignupPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { name: string, email: string, password: string } = {
    name: '',
    email: '',
    password: ''
  };

  signUperrorMessage: any = "";
  signUperrorSug: any = "";
  
  response: any = {};

  // Our translated text strings
  private signupErrorString: string;

  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService,public storage: Storage) {

    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
  }
  

  goToLoginPage(){
    this.navCtrl.push('LoginPage', {});
  }

  doSignup() {
    // Attempt to login in through our User service
    this.user.signup(this.account).subscribe((resp) => {
      this.response = resp;
      console.log(this.response);
      if(this.response.success == "success")
      {
        // set a key/value
        this.storage.set('user', this.account);
        this.storage.set('token', this.response.token).then((val)=>{
          console.log(val);
          this.navCtrl.setRoot('SettingsPage')
        });
        // this.navCtrl.push('LoginPage');
        // SettingsPage.prototype.page='profile'   
      }
      else if(this.response.message == 'User already present')
      {
          //console.log(this.response.message);
          this.signUperrorMessage = "User Already Exits";
          this.signUperrorSug = "Try Logging In";
      }
    }, (err) => {

      // Unable to sign up
      let toast = this.toastCtrl.create({
        message: this.signupErrorString,
        duration: 5000,
        position: 'top'
      });
      toast.present();
    });
  }

}
