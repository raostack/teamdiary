import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { SignupPage } from './signup';
import { ReCaptchaModule } from 'angular2-recaptcha';

@NgModule({
  declarations: [
    SignupPage,
  ],
  imports: [
    IonicPageModule.forChild(SignupPage),
    TranslateModule.forChild(),
    ReCaptchaModule
  ],
  exports: [
    SignupPage
  ]
})

export class SignupPageModule { }
