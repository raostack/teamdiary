import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LikepopoverPage } from './likepopover';

@NgModule({
  declarations: [
    LikepopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(LikepopoverPage),
  ],
  exports: [LikepopoverPage]
})
export class LikepopoverPageModule {}
