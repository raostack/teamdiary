import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController,ModalController } from 'ionic-angular';

/**
 * Generated class for the LikepopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-likepopover',
  templateUrl: 'likepopover.html',
})
export class LikepopoverPage {
  likesData=[];
  showMore=false;
  constructor(private modalCtrl: ModalController,public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    
  }
  displayLikesModal(){
    console.log(this.likesData)
    this.navCtrl.push('LikesModalPage',{data:this.likesData});
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LikepopoverPage');
    
    
    this.likesData=this.navParams.data.data;
    console.log(this.likesData);
    if(this.likesData!=null){
      if(this.likesData.length>5){
        this.showMore=true;
      }
    }
    
  }

  close() {
    this.viewCtrl.dismiss();
  }
 
}

