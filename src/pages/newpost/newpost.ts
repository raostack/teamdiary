import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { Http } from '@angular/http';
import {Api} from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { Camera, CameraOptions } from '@ionic-native/camera';
// import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
// import { File } from '@ionic-native/file';
/**
 * Generated class for the NewpostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-newpost',
  templateUrl: 'newpost.html',
})
export class NewpostPage {
  isLoggedin=true;
  newPost={};
  // fileTransfer: FileTransferObject;
  
  
  constructor(
    public navCtrl: NavController,
    public params: NavParams,
    public viewCtrl: ViewController,
    public api : Api,
    public storage: Storage,
    private camera: Camera
    // private transfer: FileTransfer, 
    // private file: File
  ){
    storage.get('user').then((user) => {
    
      
      if(user && params.data.data!=undefined && params.data.data.groupName)
      {
        let groupName=params.data.data.groupName;
        console.log("Group Name : ",groupName);
        this.newPost={
          user:{email:user.email,name:user.name},
          title:"",
          url:"",
          tags:"",
          message:"",
          security:"",
          group:groupName
        };
      }
      else{
        this.isLoggedin=false;
      }
    },error=>console.log("Error : ",error));


    console.log(params.data.data);
    // this.fileTransfer= this.transfer.create();
  }
  // upload(filepath,apiEndpoint) {
  //   let options: FileUploadOptions = {
  //      fileKey: 'file',
  //      fileName: 'name.jpg',
  //      headers: {}
  //   }
  
  //   this.fileTransfer.upload(filepath, apiEndpoint, options)
  //    .then((data) => {
  //      console.log("Upload successful",data);
  //      // success
  //    }, (err) => {
  //     console.log(err);
  //      // error
  //    })
  // }
  getPhoto() {

    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      let base64Image = 'data:image/jpeg;base64,' + imageData;

      //this.upload(imageData,null);

     }, (err) => {
       console.log(err);
      // Handle error
     });
     return true;
  }
  submitPost() {
    this.api.post("post",this.newPost,{}).subscribe(data=>console.log(data),error=>console.log(error));
    this.viewCtrl.dismiss({data:this.newPost,hasPosted:true});
  }
  dismiss(data) {
    this.viewCtrl.dismiss(data);
  }
}
