import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { NewpostPage } from './newpost';

@NgModule({
  declarations: [
    NewpostPage,
  ],
  imports: [
    IonicPageModule.forChild(NewpostPage),
    HttpModule
  ],
  exports: [
    NewpostPage
  ]
})
export class NewpostPageModule {}
