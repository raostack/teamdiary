import { Component, OnInit,OnChanges } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GroupserviceProvider } from '../../providers/providers';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the GroupsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-groups',
  templateUrl: 'groups.html',
})
export class GroupsPage implements OnInit,OnChanges {
  groupsFollowing:any = [];
  groupsAvailable:any =[];
  user:any=[];
  
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public groupService: GroupserviceProvider,public storage:Storage) {
     
  }
fetchGroups() {
  
  this.storage.get('user').then((user)=>{
    this.user=user;
    this.groupService.getFollowingGroups(user.email).subscribe((res:any)=>{
      this.groupsFollowing=res;
      console.log("No of Groups Following are "+ this.groupsFollowing.length);
    });
    this.groupService.getAvailableGroups(user.email).subscribe((res:any)=>{
      this.groupsAvailable=res;
      console.log("No of Groups Available are "+ this.groupsAvailable.length);
    })      
  },err => {console.log("Error in getting groups data")});
}


  ngOnInit(){
this.fetchGroups();
  }
  ngOnChanges(){
    alert("Chaged")
  }
  newGroup(){
    this.navCtrl.push("InputformPage");
  }
 
 

  openGroup(g) {

    // this.navCtrl.push("GroupPostsPage", { groupId: g });
    //alert("You have clicked on  "+g);
    console.log("You have clicked on  "+g);

  }
  joinGroup(groupName) {
    let userdata:any={};
    userdata.email=this.user.email;
    userdata.name=this.user.name;
    
    this.groupService.joinGroup(groupName,userdata).subscribe((res)=>{
      console.log("Joined in "+groupName)
      console.log(res);
    },err => {
      console.log("Error in Joining")
    });
    this.fetchGroups();
    

  }

  leaveGroup(groupName) {
    this.groupService.leaveGroup(groupName,this.user.email).subscribe((res)=>{
      console.log("You Left "+groupName);
    })
    this.fetchGroups();

  }

  deleteGroup(groupId){
    this.groupService.deleteGroup(groupId).subscribe((res)=>{
      console.log("Deleted the group succesfully");
      console.log(res);
    },err => {
      console.log("Error in Deleting");
    });
    this.fetchGroups();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GroupsPage');
  }

}

