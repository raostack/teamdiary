import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GroupserviceProvider } from '../../providers/providers';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the InputformPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inputform',
  templateUrl: 'inputform.html',
})
export class InputformPage implements OnInit {
  user: any = {};
  group: any = {};
  menuOption:any ;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public groupService: GroupserviceProvider, public storage: Storage) {
      this.group.members=[];
      this.group.type="corporate";
      this.group.visibility="close";
      this.group.menu =[];
      

  }

  ngOnInit() {
    this.storage.get('user').then((user) => {
      this.user = user;
    })
  }
  addOptions(){
  this.group.menu.push(this.menuOption);
  this.menuOption="";
}
  newGroup() {
    this.group.email = this.user.email;
    this.group.members.push(this.user.email);
    this.groupService.createGroup(this.group).subscribe((res: any) => {
      console.log(this.group.displayName+"Group Created");
      }
      , error => { alert("Kuch error") })
    this.navCtrl.push("GroupsPage");

  }
  check(){
    console.log(this.group);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad InputformPage');
  }

}
