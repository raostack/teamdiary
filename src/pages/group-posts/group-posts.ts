import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GroupserviceProvider } from '../../providers/providers';

/**
 * Generated class for the GroupPostsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-group-posts',
  templateUrl: 'group-posts.html',
})
export class GroupPostsPage {
  groupId:any;
  previliges:boolean = true;
  constructor(public navCtrl: NavController, public navParams: NavParams,public groupdetails: GroupserviceProvider,) {
    this.groupId = navParams.get('groupId');
    
  }
  deleteGroup(){
    this.groupdetails.deleteGroup(this.groupId).subscribe((res)=>{
      alert("Deleted group Successfully");
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GroupPostsPage');
  }

}
