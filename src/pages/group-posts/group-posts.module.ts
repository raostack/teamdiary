import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroupPostsPage } from './group-posts';

@NgModule({
  declarations: [
    GroupPostsPage,
  ],
  imports: [
    IonicPageModule.forChild(GroupPostsPage),
  ],
})
export class GroupPostsPageModule {}
