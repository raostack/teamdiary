import { Component,OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams ,ModalController} from 'ionic-angular';
import { Location } from '@angular/common';
import { GroupserviceProvider } from '../../providers/providers';
import { Storage } from '@ionic/storage';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';

/**
 * Generated class for the TestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-test',
  templateUrl: 'test.html',
})
export class TestPage implements OnInit {
  @ViewChild(Slides) slides: Slides;
  user: any;
  groupsAvailable: any = [];
  posts:any = [];
  currentIndex:any = 0;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private location: Location,
    public groupService: GroupserviceProvider, public storage: Storage,
    private modalCtrl: ModalController) {
    
  }
ngOnInit(){
  this.displayGroups();
  console.log("Indide ngOnit");
  // console.log(this.groupsAvailable);

  console.log();
}
  displayGroups() {
    this.storage.get('user').then((user) => {
      this.user = user;
      this.groupService.getAvailableGroups(user.email).subscribe((res: any) => {
        this.groupsAvailable = res;
        console.log("No of Groups Following are " + this.groupsAvailable.length);
        console.log(res);
        this.getPosts(this.groupsAvailable[0].name);
      })
    });
}

menuFunction(gname,index){
  //alert(gname+"--"+index);
  const profileModal = this.modalCtrl.create('NewpostPage',{data:{"groupName":gname}});
  console.log("Called modal");
  profileModal.present();
  profileModal.onDidDismiss(data => {
    this.getPosts(this.groupsAvailable[this.currentIndex].name);
  });
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad TestPage');
  }

  slideChanged(){
    this.currentIndex = this.slides.getActiveIndex();
    // console.log('Current index is'+this.currentIndex);
   // console.log(this.groupsAvailable[this.currentIndex].name)
    let p= this.getPosts(this.groupsAvailable[this.currentIndex].name);
    // console.log(p);
  }


  getPosts(group){
    console.log("Inside getposts "+ group);
    this.groupService.getPosts().subscribe((res)=>{
      this.posts=res;
      //console.log(this.posts);
      this.posts=this.posts.filter(function(post){
        //console.log(post.group+"--comparing--"+group);
        if(post.group == group){
          //console.log(post);
          return post;
        }
        
      });
    });
    // console.log("After filter posts are");
    // console.log(this.posts);
    return this.posts;
  }
}
