import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {

  languages: string[] = ["english us", "english uk", "japanese", "hindi", "english us", "english uk", "japanese", "hindi", "english us", "english uk", "japanese", "hindi"];
  isDisabled: boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  enableLogin() {
    this.isDisabled = false;
  }

  goToLoginPage() {
    this.navCtrl.push('LoginPage', {});
  }

  goToSignupPage() {
    this.navCtrl.push('SignupPage', {});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPasswordPage');
  }

}
