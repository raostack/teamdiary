import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker
} from '@ionic-native/google-maps';

import { Storage } from '@ionic/storage';
import { User } from '../../providers/user/user';


declare var google;

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {

  @ViewChild('map') mapElement: ElementRef;

  map: GoogleMap;
  latLng: any;
  usersLocation: any = {};
  val: any = {};
  icons: any = {};


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public geolocation: Geolocation,
    public googleMaps: GoogleMaps,
    public user: User,
    public storage: Storage) {

    user.api.get('users').subscribe((value) => {
      this.val = value;
    })
  }

  ionViewDidLoad() {
    this.loadMap();
  }

  loadMap() {

    this.geolocation.getCurrentPosition().then((resp) => {
      //console.log(resp.coords);   // resp.coords.latitude resp.coords.longitude

      let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      this.latLng = latLng;
      //console.log(this.latLng);
      let mapOptions = {
        center: latLng,
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

      this.addCurrMarker();
      this.addMarker();
    }).catch((error) => {
      console.log('Error getting location', error);
    });

  }

  addCurrMarker() {
    this.icons = {
      url: "/assets/img/dp.jpg", // url
      scaledSize: new google.maps.Size(30, 30), // scaled size
      // origin: new google.maps.Point(0, 0), // origin
      // anchor: new google.maps.Point(0, 0) // anchor
    };

    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.BOUNCE,
      icon: this.icons,
      title: 'You'
    });
    marker.setPosition(this.latLng);
  }

  addMarker() {
    for (let i = 0; i < this.val.length; i++) {
      if (!this.val[i].location) {
      }
      else {
        this.icons[i] = {
          url: "/assets/img/dp.jpg", // url
          scaledSize: new google.maps.Size(30, 30), // scaled size
          // origin: new google.maps.Point(0, 0), // origin
          // anchor: new google.maps.Point(0, 0) // anchor
        };

        let marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          icon: this.icons[i],
          title: this.val[i].name
        });
        let latLng = new google.maps.LatLng(this.val[i].location.lat, this.val[i].location.lon);
        console.log(latLng);
        marker.setPosition(latLng);
      }
    }
  }

}
