import { Component, ViewChild } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { TranslateService } from '@ngx-translate/core';
import { Config, Nav, Platform, Alert } from 'ionic-angular';
import {Storage} from '@ionic/storage'

import { LocalNotifications } from '@ionic-native/local-notifications';
import { BackgroundMode } from '@ionic-native/background-mode';


import { FirstRunPage } from '../pages/pages'
import { WelcomePage } from '../pages/pages'
import { Settings } from '../providers/providers';
import { Api } from '../providers/api/api';

import { IonicPage, MenuController, NavController } from 'ionic-angular';
import { MainPage } from '../pages/pages';

import { Network } from '@ionic-native/network';



@Component({
  template: `<ion-menu [content]="content">
    <ion-header>
      <ion-toolbar>
        <ion-title>Teamdiary</ion-title>
      </ion-toolbar>
      <div class="menu-image-div">
      <img [src]="'/assets/img/dp.jpg'" alt="User_image" class="menu-user-image">
      <h5>Name of the user</h5>
      <p>Email id of the user</p>
      </div>
    </ion-header>

    <ion-content>
      <ion-list>
        <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">
          {{p.title}}
        </button>
      </ion-list>
    </ion-content>

  </ion-menu>
  <ion-nav #content [root]="rootPage"></ion-nav>`
})

export class MyApp {
  rootPage: any;
  
  @ViewChild(Nav) nav: Nav;

  pages: any[] = [
    { title: 'Tutorial', component: 'TutorialPage' },
    { title: 'Profile', component: 'SettingsPage' },
    { title: 'Find Friends', component: 'MapPage' },
    { title: 'Tabs', component: 'TabsPage' },
    { title: 'Groups', component: 'GroupsPage' },
    { title: 'GroupPosts', component: 'GroupPostsPage' },
    { title: 'InputForm', component: 'InputformPage' },
    { title: 'Login', component: 'LoginPage' },
    { title: 'Signup', component: 'SignupPage' },
    { title: 'Logout', component: 'LogoutPage' },
    { title: 'Offline', component: 'OfflinePage' }
  ]

  constructor(private backGround:BackgroundMode,private notification:LocalNotifications,private api:Api,private network: Network,private translate: TranslateService, platform: Platform, settings: Settings, private config: Config, private statusBar: StatusBar, private splashScreen: SplashScreen,private storage: Storage) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.checkNetwork();
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });

    storage.get('status').then((val) => {

      if (val) {
        // Or to get a key/value pair
        this.storage.get('token').then((token) => {
          console.log('Token is', token);

          if (token != null) {
            this.api.setToken(token);
            this.nav.setRoot(MainPage, {}, {
              animate: true,
              direction: 'forward'
            });
          }
          else {
            this.nav.setRoot('LoginPage', {}, {
              animate: true,
              direction: 'forward'
            });
          }
        });
      }
      else {
        this.rootPage = FirstRunPage;
      }
    })
    this.initTranslate();
    this.initTimer();
  }
  initTimer(){
    this.backGround.enable();
    // console.log('Hi!');
    // setInterval(()=>{
    //   // console.log(this.i);
    //   // this.i++;
    //   this.notification.schedule({
    //     id: 1,
    //     text: 'Single ILocalNotification',
    //     sound: null
    //   })
    // },1000)
    
  }
  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');

    if (this.translate.getBrowserLang() !== undefined) {
      this.translate.use(this.translate.getBrowserLang());
    } else {
      this.translate.use('en'); // Set your language here
    }

    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  checkNetwork()
  {
      let disconnectSub = this.network.onDisconnect().subscribe(() => {
        alert('you are offline please connect to internet');
        this.nav.setRoot('OfflinePage');
      });

      let connectSub = this.network.onConnect().subscribe(()=> {
        alert('you are online');
        this.nav.setRoot(MainPage);
      });
  }

}
